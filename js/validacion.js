



//*******************************************************************

function validarElementoInput(tipo,Elemento,MensajeNegativo,filter)
{
	var bandera=0;
	var valor = Elemento.val();
	
	if(tipo=="input")
	{
		if( valor == null || valor.length == 0 || /^\s+$/.test(valor) )
		{
			Elemento.parent().attr('class', 'form-group has-error has-feedback');
			Elemento.parent().children('span.help-block').text(MensajeNegativo).show();
			Elemento.parent().children('span.glyphicon').remove();
			Elemento.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
			bandera++;
		  return bandera;
		}
		else 
			if( !filter.test(valor) )
			{
				
				Elemento.parent().attr('class', 'form-group has-error has-feedback');
				Elemento.parent().children('span.help-block').text(MensajeNegativo).show();
				Elemento.parent().children('span.glyphicon').remove();
				Elemento.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');
				bandera++;
			  return bandera;
			}
			else
			{
				Elemento.parent().attr('class', 'form-group has-success has-feedback');
				Elemento.parent().children('span.help-block').text('').hide();
				Elemento.parent().children('span.glyphicon').remove();
				Elemento.parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}

	}else if(tipo=="select"){
		varlor = Elemento.selectedIndex;
		if( valor == null || valor == 0 ) {
		Elemento.parent().attr('class', 'form-group has-error has-feedback');
		Elemento.parent().children('span.help-block').text(MensajeNegativo).show();
		Elemento.parent().children('span.glyphicon').remove();
		// Elemento.parent().append('<span class="glyphicon glyphicon-remove form-control-feedback"></span>');	
		  bandera++;
			  return bandera;
		}else
			{
				Elemento.parent().attr('class', 'form-group has-success has-feedback');
				Elemento.parent().children('span.help-block').text('').hide();
				Elemento.parent().children('span.glyphicon').remove();
				// Elemento.parent().append('<span class="glyphicon glyphicon-ok form-control-feedback"></span>');
			}
	}
	
}

//*******************************************************************
function ElementosValidar(idElemento,clase)
{
	var contador=0;
	idElemento="#"+idElemento;
	// console.log(clase);
	// console.log("Vnumero "+clase.search("Vnumero"));

	if (clase.search("Vnumero")==13)	
	 contador+=validarElementoInput("input",$(idElemento),"Ingrese solo Numeros",/^\d+$/);
	

	if (clase.search("Vtexto")==13)
	 contador+=validarElementoInput("input",$(idElemento),"Ingrese solo Texto", /^(([A-Z])|([a-z]))|\s$/);

	if (clase.search("Vdireccion")==13)
	 contador+=validarElementoInput("input",$(idElemento),"Ingrese la Dirección", /\S/g);
	
	if (clase.search("VEmail")==13)
	 contador+=validarElementoInput("input",$(idElemento),"Ingresa el formato email correcto", /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/ );

	if (clase.search("Vpass")==13)
	 contador+=validarElementoInput("input",$(idElemento),"Ingresa la Clave", /^\S+$/);


	if (clase.search("Vfecha")==13)
	{
		console.log(idElemento);
	 contador+=validarElementoInput("input",$(idElemento),"Ingrese su Fecha", /^\d|[-]+$/);
	}	

	if (clase.search("Vlista")==13)
		contador+=validarElementoInput("select",$(idElemento),"Seleccione un elemento",/^\d+$/);
}
//*******************************************************************
function ejecutarTodasValidaciones()
{
	 $(".formularioModificado").find(':input').each(function() {
         var elemento= this;
         // alert("elemento.id="+ elemento.id + ", elemento.value=" + elemento.value); 
         if(elemento.id!="")
         {
         	// alert("elemento.id="+ elemento.id + ", elemento.class=" + $("#"+elemento.id).attr('class'));
         	ElementosValidar(elemento.id, $("#"+elemento.id).attr('class'));
         }	
         
        });
}
//*******************************************************************
function BuscarErrores()
{
	var contador=0;
	if ($("div.has-error").length)
	{
       	contador++; 
    }
	 
	 // alert(contador);
	 return contador;
}
//*******************************************************************
function compararEmail(email2){
		var email1="#mailForm";
		
	if(email2=="mailForm2" || email2=="mailForm")
	{
	  email2="#mailForm2";	
	  if($(email1).val()!=$(email2).val())
	  {
	  validarElementoInput("input",$(email2),"No Coinciden las Dirrecciones Email",/^\d+$/);
	  // console.log("Resultado negativo "+$(email2).val());
	  }else
	  {
	  validarElementoInput("input",$(email2),"Ingresa el formato email correcto", /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/ );
	  // console.log("Resultado Positivo"+$(email2).val());
	  }
	}

}

function EventosEjecutanValidacion()
{
  // $(".formularioModificado").submit(function() {
  //   ejecutarTodasValidaciones();
  //     if (BuscarErrores()==0) {
  //       alert("Valor introducido no válido");		
  //       return false;
  //     } else 
  //         return true;			
  //   }); 	
    $("#btn-success").click(function(event) {
	   // event.preventDefault();
	   ejecutarTodasValidaciones();
	   var valor=BuscarErrores();
	   // alert(valor);
      if (valor!=0) {
        // alert("Valor introducido no válido");		
        return false;
      } else {
      	// $("#btn-success").submit();
          return true;	
      }
	   // alert(contador);
	});

  $("input").on('keyup', function()
  {
  	var valor = $(this).attr('id');	
  	var clase = $(this).attr('class');	
  	
  	ElementosValidar(valor, clase);
  	compararEmail(valor);
  	// console.log(valor);
  }); 	

  $("input").on('focusout', function()
  {
  	var valor = $(this).attr('id');	
  	var clase = $(this).attr('class');	
  	
  	ElementosValidar(valor, clase);
  	compararEmail(valor);
  }); 

  $("select").on('click', function()
  {
  	var valor = $(this).attr('id');	
  	var clase = $(this).attr('class');	
  	
  	ElementosValidar(valor, clase);
  }); 


  $("textarea").on('keyup', function()
  {
  	var valor = $(this).attr('id');	
  	var clase = $(this).attr('class');	
  	
  	ElementosValidar(valor, clase);
  }); 	

  $("textarea").on('focusout', function()
  {
  	var valor = $(this).attr('id');	
  	var clase = $(this).attr('class');	
  	
  	ElementosValidar(valor, clase);
  });
}
//*******************************************************************
function inicioValidacion()
{
	$("span.help-block").hide();	
	EventosEjecutanValidacion();
}
//*******************************************************************