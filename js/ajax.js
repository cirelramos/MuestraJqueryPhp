

 $(document).on('ready', function() {

  $(document).attr('title', 'CRUD Heroes');


   $('#LiInicio').click(function(event) {
   event.preventDefault();
   $.get("pagina/inicio.php",function(respuestaSolicitud){
   $("article").html(respuestaSolicitud);
   $(document).attr('title', 'CRUD Heroes');
   });//final funcion get inicio
   });//final funcion click


   $('#LiConsulta').on('click',function(event) {
   event.preventDefault();   
   $.get("pagina/consultarHeroes.php",function(respuestaSolicitud){
   $("article").html(respuestaSolicitud);
   $(document).attr('title', 'Consulta Heroes');
   });//final funcion get Consulta
   });//final funcion click
  

   $('#LiAgregar').click(function(event) {
   event.preventDefault();   
   $.get("pagina/agregarHeroes.php",function(respuestaSolicitud){
   $("article").html(respuestaSolicitud);   
   $(document).attr('title', 'Agregar Heroes');
   });//final funcion get Consulta
   });//final funcion click


   $('body').on('click','.btnConsultas',function(event) {
   event.preventDefault();
   $(document).attr('title', 'Busqueda de Heroes');
   BusquedaAjax();
   });//final funcion click


   $('body').on('submit','.formBusqueda',function(event) {
     //$('#search_form').submit(function(e) {
     event.preventDefault();
     $(document).attr('title', 'Busqueda de Heroes');
     BusquedaAjax();
   });//final de funcion submit


   $('body').on('click','.linkActualizar',function(event) {
   event.preventDefault();
   $(document).attr('title', 'Actualizar Heroes');
   CargarformAE("Actualizar",$(this).attr('href'));
   });//final funcion click

   $('body').on('click','.linkEliminar',function(event) {
   event.preventDefault();
   $(document).attr('title', 'Eliminar Heroes');
   CargarformAE("Eliminar",$(this).attr('href'));
   });//final funcion click
       
    $('body').on('submit','.formuAE',function(event) {
     //$('#search_form').submit(function(e) {
    $(document).attr('title', 'Heroes');
     event.preventDefault();
     AEAjax();
   });//final de funcion submit

    
  
    $('body').on('submit','.AgregarHeroe',function(event) {
    //$('#search_form').submit(function(e) {
    event.preventDefault();
    agregarAjax();
    });//final de funcion submit


  }); //final document ready



 


function BusquedaAjax(){
  var consultafrm;
  $.get("pagina/consultarHeroes.php",function(respuestaSolicitud){
   consultafrm=respuestaSolicitud;
   });//final funcion get Consulta
   var envio = $('#jtxtBuscarHeroe').val();
    $.ajax({
      url: 'configuracion/procesadorLlamadoClases.php',
      type: 'POST',     
      data: ({'clase':'consulta','jtxtBuscaHeroe':envio}),
      success:function(respuestaSolicitud){
        if (respuestaSolicitud!=""){
         $("article").html(consultafrm+respuestaSolicitud);
        }else{
          $("article").html("no hay ningun valor");
          
        }
      }
    });//fin de ajax
  }//fin de la funcion


function CargarformAE(tipo,envio){
    $.get('configuracion/procesadorPeticiones.php',{'info':envio,'tipo':tipo}, function(respuestaSolicitud) {
      $("article").html(respuestaSolicitud);
    });// fin del get
}//fin de la funcion



function agregarAjax(){

    var formData = new FormData(document.getElementById("formAgregarH"));     
    
    $.ajax({
      url: 'configuracion/procesadorLlamadoClases.php',
      type: 'POST',     
      //data: ({'jtxtNombre':nombre,'file_archivos':imagen,'fotoHdn':imagen2}),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success:function(respuestaSolicitud){
        if (respuestaSolicitud!=""){
         $("article").html(respuestaSolicitud);
        }else{
          $("article").html("no hay ningun valor");
          
        }
      }
    });//fin de ajax
  }//fin de la funcion




function AEAjax(){
    var formData = new FormData(document.getElementById("formAE"));     
    console.log(formData);
    $.ajax({
      url: 'configuracion/procesadorLlamadoClases.php',
      type: 'POST',     
      //data: ({'jtxtNombre':nombre,'file_archivos':imagen,'fotoHdn':imagen2}),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success:function(respuestaSolicitud){
        if (respuestaSolicitud!=""){
         $("article").html(respuestaSolicitud);
        }else{
          $("article").html("no hay ningun valor");
          
        }
      }
    });//fin de ajax
  }//fin de la funcion