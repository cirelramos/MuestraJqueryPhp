<?php 

//CONSTANTES: atributos o variables que no cambian en el transcurso de la ejecucion del programa o no son modificables

//nombre de la variable(EN MAYUSCULAS POR ESTANDAR) //valor de la variable O CONSTANTE
 define("NUMERO","10");

 //variables
//iniciamos con un simbolo de $  // nombre de la variable
 $numero2=5;


$saludos="Hola has entrado a la pagina de Variable-atributos";
//tenemos ventajas en languajes permisivos como php, js, entre otros que nos permiten declarar variables sin necesidad de colocar el tipo, como sucede en JAVA ó C++ int numero=2; varchar saludos='texto';

echo $saludos."</br>";
//etiqueta html </br> saldo de linea

$suma=$numero2+NUMERO;


echo "El resultado de la suma es ".$suma."</br>";
// para concatenar en php utilizamos el PUNTO a diferencia de otros lenguajes que se realiza con el simbolo de MAS (+)

printf("El resultado de la suma es ".$suma."</br>");
printf("El resultado de la suma es %s",$suma);
//actualmente los ECHO se utilizan solo para testear codigo y los PRINTF para imprimir resultado de los procesos.

//sprintf son utilizados para imprimir dentro de variables ( la utilizaremos mas adelante)

echo "<hr>";

// <hr> etiqueta html, linea que divide el contenido

$paredDelFor=3;

$arreglo=array(0=>"Valor 1",1=>"valor 2",2=>"valor 3");

for ($i=0; $i <$paredDelFor ; $i++) { 
	printf("%s </br>",$arreglo[$i]);
}

echo "<hr>";
$j=0;
while($j<$paredDelFor){
	echo $arreglo[$j]."</br>";
	if($arreglo[$j]=="valor 2"){
		echo "Te encontre</br> ";
		$j=5;
	}else{
		echo "Busqueda</br> ";
	}
	$j++;
}




//
 ?>