<?php 

//include("clase.php");

//los require a diferencia de los includes si se encuentra un error en la sintaxis de la pagina php, frena la ejecucion del programa.

//los require son una nueva forma de llamar otras hojas de php.
require 'clases.php';
// require_once 'file'; es utilizado para llamar una sola vez el archivo o pagina php




//para instanciar clases (llamarlas o utilizarlas), debemos ejecutar la siguiente esctructra.
//variable que captura el objeto, palabra reservada NEW y nombre del objeto u clase.
$movil = new Celular;
//para manejar las propiedades y metodos de las clases utilizamos los simbolos ->
$movil->recargarSaldo(15);
$movil->llamar();



$ginger = new Android;
//para manejar las propiedades y metodos de las clases utilizamos los simbolos ->
$ginger->recargarSaldo(15);
$ginger->llamar();
 ?>