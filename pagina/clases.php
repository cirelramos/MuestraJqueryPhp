<?php 

//las clases: son un conjunto de atributos y metodos que nos permiten interactuar con el objeto que es la clase.

//para declara las clases utilizamos la palabra reservada CLASS

// y debe ir seguida del nombre de la clase, por el estandar las clases, siempre la letra del primer nombre va en mayuscula

class Celular{
//atributos son las caracteristica de el objeto

// para declarar variables en clases utilizamos la palabra reservada VAR

var $saldo=0;
//Metodos: nos permiten Realizar Acciones sobre los objetos

function llamar(){
	if($this->saldo==0){
		echo "Saldo insuficiente";
	}else{
		echo "llamando </br>";
	}

}




function recargarSaldo($cantidad){
$this->saldo=$cantidad;
printf("Su saldo a sido recargado con %s </br>", $this->saldo);
}

}

class Android extends Celular{

function llamar(){
	echo "llamando voz ip";
}


}


 ?>