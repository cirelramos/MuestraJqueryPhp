<p>Bienvenido al CRUD de Super Heroes</p>
</br></br>
<p>Un poco de Historia por Wikipedia:</p>
</br>
<p>En la mitología y el folclore, un héroe (del griego antiguo ἥρως hērōs) o heroína (femenino) es un personaje eminente que encarna la quinta esencia de los rasgos claves valorados en su cultura de origen. Comúnmente el héroe posee habilidades sobrehumanas o rasgos de personalidad idealizados que le permiten llevar a cabo hazañas extraordinarias y beneficiosas («actos heroicos») por las que es reconocido (compárese con el villano).</p>