<?php 

require_once('conector.php');

class Busquedas extends ConexionBaseDedatos{

/*************************************************************************/
	function busquedaSelect($datos){
		$jtxtBusca=$datos;
		//if(isset($_POST['btnConsultar']))/*verificar si se ejecuto el boton*/
		$jtxtBusca= $_POST['jtxtBuscaHeroe'];/*obtener los datos de la caja de texto*/		
		$conexion=$this->ConectarBasedeDatos();/*insertar el metodo conectar vase de datos en una variable*/
		$sql="";
		//$sql="SELECT * FROM superheroes WHERE ";
		$sql="SELECT * FROM superheroes WHERE id LIKE '%".$jtxtBusca."%' OR nombre LIKE '%".$jtxtBusca."%' OR historia LIKE '%".$jtxtBusca."%'";/*crear la sentencia sql y almacenarla en una variable*/
		$ejecutarConsulta=$conexion->query($sql);/*ejecutar la sentencia sql y el retorno de la funcion introducirlo en una variable*/
		if(!$ejecutarConsulta){/*verificamos si se ejecuta bn la sentencia, la variable sin comparadores nos retorna automaticamente un true o false asi que no es necesario pero se puede colocar*/
			printf("problema con el servidor: ".$conexion->error);
		}else{
			$num_regs = $ejecutarConsulta->num_rows;/*obtengo la cantidad de resultados*/
			//if ($datos=="" && isset($_POST['btnConsultar'])) {/*verificos si las variables son correctas*/
				
				/*creamos una variable y luego concatenamos el mensaje y dentro del mensaje insertamos etiquetas html que le daran forma al mismo*/

				$respuesta="";
					$respuesta.='<table class="tabla">';
					$respuesta.='<thead>';/*etiqueta de cabecera de tabla*/
					$respuesta.='<tr>';
					$respuesta.='<th class="cabecera">id</th>';
					$respuesta.='<th class="cabecera">nombre</th>';
					$respuesta.='<th class="cabecera">imagen</th>';
					$respuesta.='<th class="cabecera">historia</th>';
					$respuesta.='<th class="cabecera"></th>';
					$respuesta.='<th class="cabecera"></th>';
					$respuesta.='</tr>';	
					$respuesta.='</thead>';



					$respuesta.='<tbody>';/*etiqueta de cuerpo de tabla*/
					
				while ($arreglo=$ejecutarConsulta->fetch_assoc()) {
					$id=$arreglo['id'];
					$nombre=$arreglo['nombre'];
					$imagen=$arreglo['imagen'];
					$historia=$arreglo['historia'];	
					$direccionActualizar=$id;
					$direccionEliminar=$id;
					$respuesta.='<tr>';/*etiqueta de fila de tabla*/
					$respuesta.='<td class="bId">'.$id.'</td>';/*etiqueta de celda*/
					$respuesta.='<td class="bNombre">'.$nombre.'</td>';
					$respuesta.='<td class="bImagen"><img src="'.$imagen.'" alt=""></td>';
					$respuesta.='<td class="bhistoria">'.$historia.'</td>';			
					$respuesta.='<td class="cabecera"><a href="'.$direccionActualizar.'" class="linkActualizar">Actualizar</a></td>';
					$respuesta.='<td class="cabecera"><a href="'.$direccionEliminar.'" class="linkEliminar">Eliminar</a></td>';
					$respuesta.='</tr>';
				}
					
				$respuesta.='</tbody>';
				$respuesta.='</table>';
				printf(utf8_encode($respuesta));/*utilizamos la funcion pritnf y encode para darle formato americanos de texto sobre la variable que concatenamos antes para imprimirla*/
				
				
				return $num_regs; /*retornamos el numero de registros obtenidos*/

			
		}
	
	
	$conexion->close();/*cerramos la conexion*/
	}


	function BusquedaArreglo($dato){
		$conexion=$this->ConectarBasedeDatos();/*insertar el metodo conectar vase de datos en una variable*/
		$sql="";
		//$sql="SELECT * FROM superheroes WHERE ";
			$sql="SELECT * FROM superheroes WHERE id='".$dato."'";
		$ejecutarConsulta=$conexion->query($sql);
		if(!$ejecutarConsulta){/*verificamos si se ejecuta bn la sentencia, la variable sin comparadores nos retorna automaticamente un true o false asi que no es necesario pero se puede colocar*/
			die("problema con el servidor: ".$conexion->error);
		}else{

			$arregloRetorno;
			while ($arreglo=$ejecutarConsulta->fetch_assoc()) {
			$arregloRetorno=array("id"=>$arreglo["id"],
			 "nombre"=>$arreglo["nombre"],
			  "imagen"=>$arreglo["imagen"],
			   "historia"=>$arreglo["historia"]);

		  }

		return $arregloRetorno;	
		}

	}




}
/*************************************************************************/

class Registro extends Busquedas{
	function AgregarHeroes(){
			$mensaje="";
			$nombre= $_POST['jtxtNombres'];	
			$texto= $_POST['txtHistoria'];
			$tipo = $_FILES["file_archivos"]["type"];
			$archivo = $_FILES["file_archivos"]["tmp_name"];
		    $se_subio_imagen = $this->subir_imagen($tipo,$archivo,$nombre);
		    $imagen = empty($archivo)?$this->imagenDefault:$se_subio_imagen;
	//$this->busquedaSelect('');
			$sql= "INSERT INTO superheroes (nombre,imagen,historia) values('$nombre','$imagen','$texto') ";
		$conexion=$this->ConectarBasedeDatos();	
		$ejecutarConsulta = $conexion->query(utf8_decode($sql));
		if($ejecutarConsulta){
			$mensaje.="Se Registro el Heroe";
		}else{
			$mensaje="problema con el servidor: ".$conexion->error;
		}
			$conexion->close();
			printf($mensaje);
		//header("location: index.php?q=resultados&info=".$mensaje);/*redireccionamos y mandamos un mensaje por Get, para que cuando cage esa pagina muestre el msaje enviado*/

	
	
}
}

/****************************************************************************/

class Modificar extends Registro{
function modificarHeroe(){
	$mensaje="";
	$id= $_POST['heroeId'];	
	$nombre= $_POST['jtxtNombreActualizar'];	
	$texto= $_POST['txtHistoria'];
	$imagen="";
	if(empty($_FILES["file_archivos"]["tmp_name"])){
				$imagen = $_POST["fotoHdn"];
			}else{
				$tipo = $_FILES["file_archivos"]["type"];
				$archivo = $_FILES["file_archivos"]["tmp_name"];
				$imagen=$this->subir_imagen($tipo,$archivo,$nombre);
			}
	$sql="UPDATE superheroes SET nombre='$nombre',imagen='$imagen',historia='$texto' WHERE id='$id'";
	$conexion=$this->ConectarBasedeDatos();
	$ejecutarConsulta = $conexion->query(utf8_decode($sql));
	if($ejecutarConsulta){
			$mensaje.="Se Actualizo el Heroe";
		}else{
			$mensaje="problema con el servidor: ".$conexion->error;
		}
			$conexion->close();
			printf($mensaje);
		//header("location: index.php?q=resultados&info=".$mensaje);


}
}

/****************************************************************************/

class Eliminar extends Modificar{

function eliminarHeroe(){

$mensaje="";
$id= $_POST['heroeId'];	
$imagen = $_POST["fotoHdn"];
$imagen2="../".$imagen;
$sql="DELETE FROM superheroes WHERE id='$id'";
$conexion=$this->ConectarBasedeDatos();
	$ejecutarConsulta = $conexion->query(utf8_decode($sql));
	if($ejecutarConsulta){
		if($imagen!=$this->imagenDefault)
		$this->borrar_imagenes($imagen2,"todas");

			$mensaje.="Se Elimino el Heroe";
		}else{
			$mensaje="problema con el servidor: ".$conexion->error;
		}
			$conexion->close();
		printf($mensaje);
		//header("location: index.php?q=resultados&info=".$mensaje);



}


}

 ?>
