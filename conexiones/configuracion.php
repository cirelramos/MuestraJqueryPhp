<?php 

class DatosConexion{
 protected	$servidor="localhost";/*variables protected en clases solo pueden ser utilizadas dentro de la clase y sus herederas mas no fuera de ellas*/
 protected	$usuario="root";
 protected  $clave="servidor";
 protected  $bd="heroes";
 protected  $imagenDefault="imagenes/desconocido.jpg";

 /*el parametro de $extension determina que tipo de imagen no se borrar por ejemplo si es jpg significa que la imagen con extension .jpg se queda en el servidor y si existen imagenes con el mismo nombre pero con extension png o gif se eliminaran con esta funcion evito tener las imagenes duplicadas con distinta extension para cada perfil la funcion file exists evalua si un archivo existe y la funcion unlink borra un archivo del servidor*/	

 function borrar_imagenes($ruta,$extension){
 	switch ($extension) {
 		case ".jpg":
 			if(file_exists($ruta.".png") )
 				unlink($ruta.".png");
 			if(file_exists($ruta.".gif") )
 				unlink($ruta.".gif");
 			break;
 		case ".gif":
 			if(file_exists($ruta.".png") )
 				unlink($ruta.".png");
 			if(file_exists($ruta.".jpg") )
 				unlink($ruta.".jpg");
 			break;
 		case ".png":
 			if(file_exists($ruta.".jpg") )
 				unlink($ruta.".jpg");
 			if(file_exists($ruta.".gif") )
 				unlink($ruta.".gif");
 			break;
 			default:
 			unlink($ruta);
 			break;
 	}
 }




 //función para  subir la imagen del perfil de usuario
 function subir_imagen($tipo,$imagen,$nombre){
 	//strstr ($cadena1,$cadena) sirve para evaluar si en la primer cadena de texto existe la segunda cadena de texto si dentro del tipo de archivo se encuentra la palabra imagen significica que el archivo es una imagen

 	if(strstr($tipo,"image")){  
 		//el archivo si es una imagen, para saber q tipo de extension es la imagen
 		if(strstr($tipo,"jpeg"))
 			$extension = ".jpg";
 		else if(strstr($tipo,"gif"))
 			$extension = ".gif";
 		else if(strstr($tipo,"png"))
 			$extension = ".png";

 		
 			//guardo la ruta que tendra el servidor la imagen
 			$destino="../imagenes/".$nombre;
 			//se sube la foto
 			move_uploaded_file($imagen, $destino.$extension) or die ("no se pudo subir la imagen al servidor =( ");
 			//ejecuto la funcion para borrar posibles imagenes dobles para perfil
 			$this->borrar_imagenes($destino,$extension);
 			$destino="imagenes/".$nombre;
 		//asigno el nombre de la foto que se guardara en la bd como cadena de texto
 		$imagen = $destino.$extension;
 		return $imagen;
 	}else{
 	return false;
 	}
 }



}






 ?>