-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 19-06-2015 a las 13:19:55
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `heroes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `superheroes`
--

CREATE TABLE IF NOT EXISTS `superheroes` (
`id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `imagen` varchar(300) NOT NULL,
  `historia` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `superheroes`
--

INSERT INTO `superheroes` (`id`, `nombre`, `imagen`, `historia`) VALUES
(1, 'Batman', 'imagenes/187917.jpg', 'Bruce Wayne es un filantropo, multimillonario y presidente corporativo de las Industrias Wayne. En secreto, él encarna a Batman el héroe enmascarado que lucha contra el mundo del hampa y del crimen organizado en Ciudad Gótica combatiendo a peligrosos criminales que amenazan la paz de sus conciudadanos. Bruce juró ante la tumba de sus fallecidos padres quienes murieron en un atraco a mano armada, combatir la delincuencia en todos sus aspectos. Adoptó el disfraz de murciélago porque según él, los criminales son supersticiosos y la figura de un animal oscuro les infundiría de terror. Tiene multitud de aparatos y vehículos de alta tecnología que le ayudan en su misión de limpiar a Ciudad Gótica, además de haber tenido varios ayudantes: Robins y Batichicas. Su fiel mayordomo, Alfred Pennyworth, es su médico, amigo y asistente. Experto en combate cuerpo a cuerpo y maestro de diversas artes marciales, con una gran inteligencia y habilidades de detective. Es un ser humano común y corriente sin super poderes, quízas esta sea la razón por la cual es el super héroe favorito de todos los tiempos.'),
(2, 'Superman', 'imagenes/187706.jpg', 'Superman es el primer superhéroe de la historia del cómic. Con él comenzó la Edad de Oro, surgiendo muchos personajes similares o con el mismo patrón que él. Superman ha tenido muchos orígenes distintos, pero todos han tenido el mismo patrón: Superman es en realidad Kal-El, hijo de Jor-El y Lara, habitantes de Krypton. Sus padres le enviaron a la Tierra justo antes de que explotara Krypton. Cuando Kal-El llegó a la Tierra le encontraron dos estadounidenses: Jonathan y Martha Kent, una pareja que no había podido tener hijos y le adoptaron como a un hijo. Kal-El fue llamado Clark Kent y gracias al Sol amarillo de nuestro planeta pudo desarrollar superpoderes, como la velocidad de la luz, superfuerza o la habilidad de volar. Adoptó la identidad de Superman para poder defender a los indefensos. La S del pecho no es una s humana sino el símbolo de la Casa de El, de su legado kryptoniano.\r\n'),
(21, 'gta', 'imagenes/gta.jpg', 'juego de mundo abierto'),
(22, 'desconocido', 'imagenes/desconocido.jpg', 'sin historia');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `superheroes`
--
ALTER TABLE `superheroes`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `superheroes`
--
ALTER TABLE `superheroes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
