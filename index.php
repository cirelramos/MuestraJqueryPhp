<?php 
error_reporting(E_ALL^E_NOTICE);
$op=$_GET["q"];
$tipo=$_GET["tipo"];
switch ($op) {
	case 'variables':
		$contenido="pagina/variables-atributos.php";
		$titulo="Constantes, Variables, Operadores, ciclos repetitos y funciones";
	break;
	
	case 'poo':
		$contenido="pagina/llamado-de-clases.php";
		$titulo="Programación Orientada a Objetos (POO)";
	break;

	case 'consulta':
		$contenido="pagina/consultarHeroes.php";
		$titulo="Consultar Heroes";
	break;

	case 'agregar':
		$contenido="pagina/agregarHeroes.php";
		$titulo="Agregar Heroes";
	break;

	case 'resultados':
		$contenido="pagina/resultados.php";
		$titulo="Resultados";
	break;


	case 'procesador':
		$contenido="configuracion/procesadorPeticiones.php";
		$titulo=$tipo." Heroes";
	break;

	default:
		$contenido="pagina/inicio.php";
		$titulo="";
		break;
}

 ?>


<!DOCTYPE html>
<html lang="es-VEN"> <!-- nos permite especificar el lenguaje de la pagina -->
<head>
<!-- http://curso-2015.2fh.co/ -->
	<!-- reconocimiento  -->
	<meta charset="UTF-8">
	
	<!-- para especificar o describir el contenido de la Hoja actual. -->
	<meta name="description" content="Curso de php, html, css">
	<!-- etique tittle nos permite especificar un titulo a la pagina -->
	<title><?php echo $titulo ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="img/super-heroes.png">
	<link rel="stylesheet" href="css/estilos.css">
	<link rel="stylesheet" href="css/estilosBarra.css">
	<link rel="stylesheet" href="css/estilosConsultas.css">
	<link rel="stylesheet" href="css/estilosIconos.css">
	<script src="js/jquery-2.1.4.js"></script>	
	<script src="js/jquery.tooltipster.js"></script>
	<script src="js/ajax.js"></script>
</head>
<body>
	<!-- header se puede aplicar o utilizar banner o imaganes que representen a la pagina -->
	<header >
		<img src="img/super-heroes.png" alt="">
		<p>CRUD Heroes</p>
	</header>
	<!-- es utilizado para aplicar listas desordenadas -->
	<nav class="nav">
		<ul>
			<li><a href="?q=inicio" id="LiInicio">Inicio</a></li>
			<li><a href="?q=consulta" id="LiConsulta">Consulta</a></li>
			<li><a href="?q=agregar" id="LiAgregar">Agregar</a></li>
		</ul>

	</nav>
	<!-- section>article+aside -->
	<!-- (section>article)+aside -->
	<!-- section  -->

	<section id="principal">
		<section class="contenido">
	
	<!-- para especificar que en el elemento van las entradas o la informacion de la pagina -->
		<article>
			<?php include ("$contenido"); ?>
		</article>
		<hgroup>			
		</hgroup>
		<!-- para instalar un elemento como el twitter o publicaciones recientes  -->
		
	</section>
	<aside>
	<p class="textoLeyenda">Esta pagina es un desarrollo como muestra de mis capacidades implementando PHP, Mysql, Jquery, HTML5 y CSS3, lo invito a usarla y probar su funcionalidad.
	</br></br>
	Las caracteristicas mas importantes de esta muestra es que no necesita ser recargada para sus operaciones gracias AJAX y que es resposive por lo que puede ser usada en navegadores moviles como de computadores.</p>
    


    <hr>
	




	</aside>
	</section>
		
	<!-- se espcifca los derecho de autor y algunos casos se realiza un site map de la pagina -->
	<footer>

	Desarrollado por Cirel Ramos
		

<br/>
<br/>
<div class="inicio">
	<div class="vista">
		<a href="http://cirelramos.blogspot.com/"target="_blank"><img src="img/blogger-mini.png" alt="Mi Blog" ></a>
		  <div class="mascara"> 
	<a class="enlace" href="http://cirelramos.blogspot.com/"target="_blank">
	<h2>Mi Blog</h2>
	<p>Publicaciones Referentes a Gnu/Linux, que son de utilidad en la practica y uso del sistema</p>
	</a>   
	</div>      
	</div>




	


	<div class="vista">
	<a href="http://cirelramos.ml"target="_blank"><img src="img/curriculum.png" alt="curriculum" ></a>
	 <div class="mascara">
	<a class="enlace" href="http://cirelramos.ml"target="_blank"><h2>Curriculum</h2>
	<p>Accede y conoce mas sobre mis conocimientos en el Area Informática!</p>
	</a>  
	</div>
	</div>


	<div class="vista">
	<a href="http://cirelramos.ml/index.php?op=x&mensaje=inicio#email"target="_blank"><img src="img/email.png" alt="curriculum" ></a>
	 <div class="mascara">
	<a class="enlace" href="http://cirelramos.ml/index.php?op=x&mensaje=inicio#email"><h2>Email</h2>
	<p>Contatame por medio de un Correo Electronico :D </p>
	</a>  
	</div>
	</div>
</div>



	</footer>
</body>
</html>