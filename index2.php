<!DOCTYPE html>
<html lang="es-VEN"> <!-- nos permite especificar el lenguaje de la pagina -->
<head>
<!-- http://curso-2015.2fh.co/ -->
	<!-- reconocimiento  -->
	<meta charset="UTF-8">
	<!-- para especificar o describir el contenido de la Hoja actual. -->
	<meta name="description" content="Curso de php, html, css">
	<!-- etique tittle nos permite especificar un titulo a la pagina -->
	<title>Indice del curso</title>
	<link rel="stylesheet" href="css/estilos.css">
</head>
<body>
	<!-- header se puede aplicar o utilizar banner o imaganes que representen a la pagina -->
	<header >
		Esto es un un &lt;header&gt;
	</header>
	<!-- es utilizado para aplicar listas desordenadas -->
	<nav>
		&lt;nav&gt;
	</nav>
	<!-- section>article+aside -->
	<!-- (section>article)+aside -->
	<!-- section  -->

	<section id="principal">
		<section class="contenido">
	&lt;section -> contenido&gt;
	<!-- para especificar que en el elemento van las entradas o la informacion de la pagina -->
		<article>
			&lt;article&gt;
		</article>
		<hgroup>			
		</hgroup>
		<!-- para instalar un elemento como el twitter o publicaciones recientes  -->
		
	</section>
	<aside>&lt;aside&gt;
	<br/><br/>
	<a href="pagina/variables-atributos.php">Constantes, Variables, Operadores, ciclos repetitos y funciones </a>
	<br/><br/>
	<a href="pagina/llamado-de-clases.php"> 
	Programación Orientada a Objetos (POO)
	</a>
	<br/><br/>
<a href="http://google.com" target="_blank"> Google</a>

	</aside>
	</section>
		
	<!-- se espcifca los derecho de autor y algunos casos se realiza un site map de la pagina -->
	<footer>&lt;footer&gt;</footer>
</body>
</html>